package models;

import java.util.ArrayList;
import java.util.List;
import play.data.validation.Constraints;
import java.util.LinkedList;
import play.mvc.PathBindable;
import play.db.ebean.Model;
import javax.persistence.*;
import com.avaje.ebean.Page;

@Entity
public class Product extends Model implements PathBindable<Product> {
    public static Finder<Long,Product> find = new Finder<Long,Product>(Long.class, Product.class);
    
    private static List<Product> products;
  
  @Id
  public Long id;
  
  @Constraints.Required
  public String ean;
  
  @Constraints.Required
  public String name;
  
  public String description;
  
  @ManyToMany
  public List<Tag> tags = new LinkedList<Tag>();
  
  public byte[] picture;
  
  @OneToMany(mappedBy="product")
  public List<StockItem> stockItems;
  
  public Product() {}
  public Product(String ean, String name, String description) {
    this.ean = ean;
    this.name = name;
    this.description = description;
  }
  public String toString() {
    return String.format("%s - %s", ean, name);
  }
  
  public static List<Product> findAll() {
    return find.all();
  }
 
  public static Product findByEan(String ean) {
    return find.where().eq("ean", ean).findUnique();
  }
 
  public static List<Product> findByName(String term) {
    final List<Product> results = new ArrayList<Product>();
    for (Product candidate : products) {
      if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
        results.add(candidate);
      }
    }
    return results;
  }
  
  public static Page<Product> find(int page) {
   return find.where()
              .orderBy("id asc")
              .findPagingList(10)
              .setFetchAhead(false)
              .getPage(page);
  }
  
  public void save() {
    products.remove(findByEan(this.ean));
    products.add(this);
  }
  
  @Override
    public Product bind(String key, String value) {
        return findByEan(value);
    }
 
    @Override
    public String unbind(String key) {
        return ean;
    }
 
    @Override
    public String javascriptUnbind() {
        return ean;
    }
}
