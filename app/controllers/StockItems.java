package controllers;

import play.mvc.*;
import models.*;
import java.util.List;

public class StockItems extends Controller {
  public static Result index() {
    List<StockItem> items = StockItem.find
            .where()
            .ge("quantity", 300)
            .orderBy("quantity")
            .setMaxRows(5)
            .findList();
    return ok(items.toString());
  }
}
